# -*- coding: utf-8 -*-
# vim: ft=sls

{% from "node_exporter/map.jinja" import node_exporter with context %}

{% if node_exporter.service_user != 'root' %}

node_exporter-create-user:
  user.present:
    - name: {{ node_exporter.service_user }}
    - shell: /bin/false
    - system: True
    - home: /dev/null
    - createhome: False

{% endif %}


{% if node_exporter.service_group != 'root' %}

node_exporter-create-group:
  group.present:
    - name: {{ node_exporter.service_group }}
    - system: True
    - members:
      - {{ node_exporter.service_user }}
    - require:
      - user: {{ node_exporter.service_user }}

{% endif %}

node_exporter-bin-dir:
  file.directory:
   - name: {{ node_exporter.bin_dir }}
   - makedirs: True

node_exporter-dist-dir:
 file.directory:
   - name: {{ node_exporter.dist_dir }}
   - makedirs: True

node_exporter-config-dir:
  file.directory:
    - name: {{ node_exporter.config_dir }}
    - makedirs: True

node_exporter-textfile-collector-dir:
  file.directory:
    - name: {{ node_exporter.config_dir }}/textfile
    - makedirs: True

{% set inventory = salt['pillar.get']('inventory', default={'datacenter': 'local'}) %}
node_exporter-dc-metric:
  file.managed:
    - name: {{ node_exporter.config_dir }}/textfile/dc.prom
    - contents: datacenter{datacenter="{{ inventory.get('datacenter', 'undefined') }}"} 1

{% if (salt['grains.get']('fqdn').split('.')|length) == 4 %}
  {% if salt['grains.get']('fqdn').split('.')[1] in ['dev', 'test', 'int', 'stg', 'prod'] %}
node_exporter-env-metric:
  file.managed:
    - name: {{ node_exporter.config_dir }}/textfile/env.prom
    - contents: env{env="{{ salt['grains.get']('fqdn').split('.')[1] }}"} 1
  {% endif %}
{% endif %}

node_exporter-required-packages:
  pkg.installed:
    - pkgs:
      - python-pycurl
      - python-tornado
    {# Forcing the Salt minion to reload its modules for it to recognize that the proper dependencies are in place to download the node_exporter #}
    - reload_modules: True

node_exporter-install-binary:
  archive.extracted:
    - name: {{ node_exporter.dist_dir }}/node_exporter-{{ node_exporter.version }}
    - source: https://github.com/prometheus/node_exporter/releases/download/v{{ node_exporter.version }}/node_exporter-{{ node_exporter.version }}.{{ grains['kernel'] | lower }}-{{ node_exporter.arch }}.tar.gz
    - source_hash: https://github.com/prometheus/node_exporter/releases/download/v{{ node_exporter.version }}/sha256sums.txt
    - options: --strip-components=1
    - user: root
    - group: root
    - enforce_toplevel: False
    - require:
      - pkg: node_exporter-required-packages
    - unless:
      - '[[ -f {{ node_exporter.dist_dir }}/node_exporter-{{ node_exporter.version }}/node_exporter ]]'
  file.symlink:
    - name: {{ node_exporter.bin_dir }}/node_exporter
    - target: {{ node_exporter.dist_dir }}/node_exporter-{{ node_exporter.version }}/node_exporter
    - mode: 0755
    - unless:
      - '[[ -f {{ node_exporter.bin_dir }}/node_exporter ]] && {{ node_exporter.bin_dir }}/node_exporter --version | grep {{ node_exporter.version }}'

node_exporter-install-service:
  file.managed:
{% if grains['init'] == 'systemd' %}
    - name: /etc/systemd/system/node_exporter.service
    - source: salt://node_exporter/files/node_exporter.service.j2
{% else %}
    - name: /etc/init.d/node_exporter
    - source: salt://node_exporter/files/initscript.j2
    - mode: 755
{% endif %}
    - template: jinja
    - context:
        node_exporter: {{ node_exporter }}
    - require:
        - archive: node_exporter-install-binary
  service.running:
    - name: node_exporter
    - enable: True
    - start: True
    - require:
      - file: node_exporter-install-service

node_exporter-service-reload:
  service.running:
    - name: node_exporter
    - enable: True
    - watch:
      - file: node_exporter-install-service
