def test_binary_absent(File):
    file = File('/usr/bin/node_exporter')
    assert not file.exists

def test_config_file_absent(File):
    file = File('/etc/node_exporter/textfile')
    assert not file.exists

def test_config_dir_absent(File):
    file = File('/etc/node_exporter')
    assert not file.exists

def test_opt_dir_not_present(File):
    file = File('/opt/node_exporter/dist')
    assert not file.exists

def test_service_definition_removed(File):
    file = File('/etc/systemd/system/node_exporter.service')
    assert not file.exists

def test_service_is_not_running_and_disabled(Service):
    node_exporter = Service('node_exporter')
    assert not node_exporter.is_running
    assert not node_exporter.is_enabled
