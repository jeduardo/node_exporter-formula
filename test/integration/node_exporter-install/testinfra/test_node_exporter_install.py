def test_binary_created(File):
    file = File('/usr/bin/node_exporter')
    assert file.exists
    assert file.is_symlink
    assert file.user == 'root'
    assert file.group == 'root'

def test_textfile_dir_created(File):
    file = File('/etc/node_exporter/textfile')
    assert file.exists
    assert file.is_directory
    assert file.user == 'root'
    assert file.group == 'root'
    assert oct(file.mode) == '0755'

def test_dc_metric_created(File):
    file = File('/etc/node_exporter/textfile/dc.prom')
    assert file.content_string == 'datacenter{datacenter="local"} 1\n'

def test_service_definition_created(File):
    file = File('/etc/systemd/system/node_exporter.service')
    assert file.exists
    assert file.is_file
